#!/bin/bash

set -e


cp system/controlDictRun system/controlDict
decomposePar
mpiexec -np 24 interReactiveTransportFoam -parallel
reconstructPar
rm -rf processor*
